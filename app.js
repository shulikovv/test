const config = require('./config/config');
const async = require('async');
const logger = require('log4js').getLogger('[app]');
const  _ = require('underscore');
global.workers = {};
async.series({
  db: function (done) {
    const _worker = require('./workers/db');
    global.workers.db = new _worker(config)
      .setup()
      .start(done);
  },
  express: function (done) {
    const _worker = require('./workers/express');
    global.workers.express = new _worker(config)
      .setup()
      .start(done);
  },
}, function (err, result) {
  if (err) {
    throw new Error(e);
  }
  for (let idx in result) {
    logger.info(idx, result[idx]);
  }
  logger.info('Application started successfuly');
});

//graceful shutdown
function graceful() {
  logger.warn('Got termination signal');
  let series = [];
  for (let wIdx in global.workers) {
    (function (wIdx) {
      series.push(function (done) {
        global.workers[wIdx].stop(done);
      });
    })(wIdx);
  }
  async.series(series, function (err, result) {
    for (let idx in result) {
      logger.info(_.keys(global.workers)[idx], result[idx]);
    }
    logger.info('Application stopped gracefuly.');
    process.exit(0);
  })
}
process.on('SIGTERM', graceful);
process.on('SIGINT', graceful);

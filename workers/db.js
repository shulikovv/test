/**
 * Created by spl on 12/25/15.
 */
const Db = require('../app/models');
//private
let config;
let db;

function Worker(_config) {
  config = _config;
}

Worker.prototype.setup = function () {
  return this;
}

Worker.prototype.start = function (done) {
  Db.sequelize
    .authenticate()
    .then(function () {
      done(null,'started');
    }).catch(function (e) {
      console.error("Unable to connect to the database:", e);
    done(e);
  });
  return this;
}
Worker.prototype.stop = function (done) {
  done(null, 'stopped');
  return this;
}
module.exports = Worker;
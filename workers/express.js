const express = require('express');
const bodyParser = require('body-parser');
const compress = require('compression');
const methodOverride = require('method-override');
const logger = require('log4js').getLogger('[app.config.express]');

//private area
let config;
const app = express();
function Worker(opt) {
  config = opt;
}
Worker.prototype.setup = function () {
  const env = process.env.NODE_ENV || 'development';
  app.locals.ENV = env;
  app.locals.ENV_DEVELOPMENT = env === 'development';

  app.set('views', config.root + '/app/views');
  app.engine('html', require('ejs').renderFile);
  app.set('view engine', 'html');

  app.use(bodyParser.json({limit: '1mb'}));
  app.use(bodyParser.urlencoded({
    extended: true,
    limit: '1mb'
  }));
  app.use(compress());
  /** @note we do not have static files. uncomment mext line in case of usage */
    //app.use(express.static(config.root + '/public'));
  app.use(methodOverride());

  // adding CORS support
  app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,PATCH,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Origin,X-Requested-With,Content-Type,Accept,Authorization');
    next();
  });

  require('../app/routes/index')(app);

  app.use(function (req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
      logger.error(err.stack);
      res.status(err.status || 500).send();
    });
  }

  app.use(function (err, req, res, next) {
    res.status(err.status || 500).send();
  });
  return this;
}
Worker.prototype.start = function (cb) {
  app.listen(config.port, function () {
    cb(null, 'started. Listening on port ' + config.port)
  });
  return this;
}
Worker.prototype.stop = function (cb) {
  cb(null,'stopped');
  return this;
}
module.exports = Worker;

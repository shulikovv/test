const crypto = require('crypto');
const Sequelize = require("sequelize");

module.exports = function(sequelize, DataTypes) {

  const Item = sequelize.define('Item', {
    id: {
      type: DataTypes.BIGINT,
      autoIncrement: true,
      primaryKey: true
    },
    title: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    description: {
      type: DataTypes.STRING(255),
      allowNull: true,
    },
    image: {
      type: DataTypes.STRING(255),
      allowNull: true,
    },
    created_at: {
       type: DataTypes.DATE, 
       defaultValue: Sequelize.NOW
    },
    user_id: {
      type: DataTypes.BIGINT
    },
  }, {
    timestamps: false,
    tableName: 'items',
   });

  Item.associate = function(models) {
    models.Item.belongsTo(models.User, {
      as: "user",
      foreignKey: "user_id",
      sourceKey: "id"
    });
  };

  return Item;
};

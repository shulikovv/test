const crypto = require('crypto');

module.exports = function(sequelize, DataTypes) {

  const User = sequelize.define('User', {
    id: {
      type: DataTypes.BIGINT,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    phone: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    email: {
      type: DataTypes.STRING(255),
      unique: true,
      allowNull: false,
      validate: {
        isEmail: true
      }
    },
    password: {
      type: DataTypes.VIRTUAL,
      allowNull: true
    }, // this will not be added to DB
    hashedPassword: DataTypes.STRING,
    salt: DataTypes.STRING,
  }, {
    timestamps: false,
    tableName: 'users',
    hooks: {
      beforeCreate: function(user, options) {
        try {
          user.salt = user.makeSalt();
        } catch(e) {
          console.log(e);
        }
        user.hashedPassword = user.encryptPassword(user.password, user.salt);
      },
      beforeUpdate: function(user, options) {
        if (user.password && user.password.length) {
          user.salt = user.makeSalt();
          user.hashedPassword = user.encryptPassword(user.password,user.salt);
        }
      }
    },
  });
/**
  * Overriding default method to prevent password and salt to get out
  * @returns {*}
  */
  User.prototype.toJSON = function  toJSON() {
    var values = this.get();
    delete values.hashedPassword;
    delete values.salt;
    return values;
  };
/**
  * Authenticate - check if the passwords are the same
  *
  * @param {String} plainText
  * @return {Boolean}
  * @api public
  */
  User.prototype.authenticate = plainText => {
    return this.encryptPassword(plainText) === this.hashedPassword;
  };
  /**
    * Make salt
    *
    * @return {String}
    * @api public
    */
  User.prototype.makeSalt = () => {
    return crypto.randomBytes(16).toString("base64");
  };
  /**
    * Encrypt password
    *
    * @param {String} password
    * @return {String}
    * @api public
    */
  User.prototype.encryptPassword = (password, salt) => {
    if (!password || !salt) return "";
    try {
      return crypto
        .pbkdf2Sync(password, salt, 10000, 64, "sha512")
        .toString("base64");
    } catch(e) {
      console.log(e);
    }
  };
  return User;
};

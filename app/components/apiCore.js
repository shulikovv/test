const _ = require('lodash');
const UNKNOWN_ERROR_MESSAGE = 'unknown error';

module.exports = {

  sendSuccessResponse : (response, data) => response.status(200).json(data),

  sendErrorResponse : function (response, errors, code) {
    code = code || 400;
    var formattedErrors = [];

    if (_.isArray(errors.errors)) {
      errors.errors.forEach(function (error) {
        formattedErrors.push({
          code: error.code || code,
          message: error.path + ' : ' + error.message || UNKNOWN_ERROR_MESSAGE
        });
      });
    } else if ( _.isArray(errors)) {
      errors.forEach(function (error) {
        formattedErrors.push({
          code: 'someError',
          message: error.toString() || UNKNOWN_ERROR_MESSAGE
        });
      });
    } else {
      formattedErrors.push({
        code: 'someError',
        message: errors.toString() || UNKNOWN_ERROR_MESSAGE
      });
    }

    return response.status(code).json({errors:formattedErrors});
  }

};

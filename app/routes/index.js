const express = require("express");
const controller = require("../controllers/users");
const auth = require("../auth/auth.service");
const router = express.Router();

const API_PATH_SUFFIX = '/api';

let config = require('../../config/config');


module.exports = function(app) {

  app.use(function (req, res, next){
    // .... do something )))
    next();
  });

  //app.use('/auth', require('../auth'));

  //different api health utils
  app.get(API_PATH_SUFFIX + '/ping', require('../controllers/utils').ping);

  //

  // Insert routes below
  app.use(API_PATH_SUFFIX + '/',     require('./router'));

};

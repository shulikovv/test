const express = require('express');
const cUsers = require('../controllers/users');
const cItems = require('../controllers/items');
const auth = require('../auth/auth.service');
const router = express.Router();
var multer = require("multer");
var upload = multer({dest: "uploads/"});

router.post("/login", cUsers.login);
router.post("/register", cUsers.create);
router.get("/me", auth.isAuthenticated(), cUsers.me);
router.put("/me", auth.isAuthenticated(), cUsers.update);

router.get("/user/:id", auth.isAuthenticated(), cUsers.userById);
router.get("/user", auth.isAuthenticated(), cUsers.userSearch);

router.post("/item/:id/image", upload.single('photos'), cItems.createImage);
router.get("/item/:id", auth.isAuthenticated(), cItems.show);
router.put("/item/:id", auth.isAuthenticated(), cItems.update);
router.delete("/item/:id", auth.isAuthenticated(), cItems.delete);
router.get("/item", auth.isAuthenticated(), cItems.index);
router.post("/item", auth.isAuthenticated(), cItems.create);

module.exports = router;

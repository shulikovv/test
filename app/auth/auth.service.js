const sequelize = require('sequelize');
const config = require('../../config/config');
const jwt = require('jsonwebtoken');
const expressJwt = require('express-jwt');
const db = require('../models');
const compose = require('composable-middleware');
const validateJwt = expressJwt({
  secret: config.session.secret
  //credentialsRequired: false
});
const is = require('is_js');
const logger = require('log4js').getLogger('[app.auth]');
/**
 * Attaches the user object to the request if authenticated
 * Otherwise returns 401
 */
const isAuthenticated = () => {
  return compose()
      // Validate jwt
      .use(function(req, res, next) {
        // allow access_token to be passed through query parameter as well
        if (!config.auth.required) {
          return next();
        }
        if(req.query && req.query.hasOwnProperty('access_token')) {
          req.headers.authorization = req.query.access_token;
        }
        if (is.not.propertyDefined(req.headers, "authorization")) {
          return res.status(401).send();
        } else {
          //validateJwt(req, res, next);
          jwt.verify(req.headers.authorization, config.session.secret, (err, decoded) => {
            if (err) {
              return res.status(401).send();
            } else {
              req.decoded = decoded;
              return next();
            }
          });
        }
      })
      .use(function(req, res, next) {
        if (!config.auth.required) {
          return next();
        }
        db.User.findOne({ where: { id: req.decoded.id } })
          .then(function(user) {
            if (!user) return res.status(401).send();
            req.user = user;
            return next();
          })
          .catch(function(err) {
            logger.info("Auth error:", err.stack);
            return next(err);
          });
      });
}

/**
 * Returns a jwt token signed by the app secret
 */
const signToken = (id) => {
  return jwt.sign({ id: id }, config.session.secret, {
    expiresIn: config.session.tokenValidityTime+'m' || (60 * 5)+'m'
  });
}

module.exports.isAuthenticated = isAuthenticated;
module.exports.signToken = signToken;

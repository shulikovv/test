const db = require('../models');
const _ = require('lodash');
const async = require('async');
const is = require('is_js');


const ItemController = {
  index: function(request, response) {
    let itemQuery = request.query;
    let query = {};
    let _order = ["created_at", "DESC"];
    if (itemQuery.title) {
      query.title = { $like: `%${itemQuery.title}%` };
    }
    if (itemQuery.user_id) {
      query.user_id = itemQuery.user_id;
    }
    if (itemQuery.order_by && itemQuery.order_by === "title") {
      _order[0] = "title";
    }
    if (itemQuery.order_type && itemQuery.order_type === "asc") {
      _order[1] = "ASC";
    }
    db.Item.findAll({
      where: query,
      include: [
        {
          model: db.User,
          as: "user",
          attributes: ["id", "name", "email", "phone"]
        }
      ],
      order: [_order]
    })
      .then(function(items) {
        return response.json(items);
      })
      .catch(function(error) {
        return response.status(400).json(error);
      });
  },
  create: function(request, response) {
    let itemData = request.body;
    itemData.user_id = request.user.id;
    itemData.created_at = Date.now();
    db.Item.create(itemData)
      .then(function(item) {
        return response.status(201).json(item);
      })
      .catch(function(err) {
        if (err.name === "SequelizeValidationError") {
          let errors = err.errors.map(item => ({
            field: item.path,
            message: item.message
          }));
          return response.status(422).json(errors);
        }
        return response.status(500).json();
      });
  },
  show: function(request, response) {
    const itemId = 1 * request.params.id;

    db.Item.findOne({ id: itemId })
      .then(function(item) {
        if (!item) {
          return response.status(404).json({ message: "Not Found" });
        }
        return response.json(item);
      })
      .catch(function(error) {
        return response.status(400).json(error);
      });
  },
  delete: function(request, response) {
    db.Item.findOne({ where: { id: request.params.id } }).then(function(item) {
      if (!item) {
        return response.status(404).json("");
      }
      item
        .destroy()
        .then(function() {
          return response.status(200).json("");
        })
        .catch(function(error) {
          return response
            .status(400)
            .json({ message: error.message || "Error occured" });
        });
    });
  },
  update: function(request, response, next) {
    // title, description
    const itemData = request.body;
    const itemId = 1 * request.params.id;
    db.Item.findOne({ where: { id: itemId } })
      .then(function(item) {
        if (!item) {
          return response.status(404).json({ message: "Not Found" });
        }

        item.title = itemData.title || item.title;
        item.description = itemData.description || item.description;
        return item
          .save()
          .then(function(result) {
            return response.status(200).json(item);
          })
          .catch(function(error) {
            if (err.name === "SequelizeValidationError") {
              let errors = err.errors.map(item => ({
                field: item.path,
                message: item.message
              }));
              return response.status(422).json(errors);
            }
            return response.status(500).json();
          });
      })
      .catch(function(err) {
        return next(err);
      });
  },
  createImage: function(request, response, next) {
    return response.status(404).json();
  }
};

module.exports = ItemController;

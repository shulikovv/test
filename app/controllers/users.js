const db = require('../models');
const _ = require('lodash');
const async = require('async');
const auth = require('../auth/auth.service');

const UserController = {
  create: function(request, response) {
    const userData = request.body;
     return db.User.find({
        where: { email: userData.email }
      }).then((u) => {
        if (u) {
          return response
            .status(422)
            .json([
              { field: "email", message: "This email already exists" }
            ]);
        }
        db.User.create(userData)
          .then(function(user) {
            let token = auth.signToken(user.id);
            return response.status(200).json({token:token});
          })
          .catch(err => {
            if (err.name === "SequelizeValidationError") {
              let errors = err.errors.map(item => ({
                'field': item.path,
                'message': item.message
              }))
              return response
                .status(422)
                .json(errors);                
            }
            return response
                .status(500)
                .json();
          });
      });

  },
  update: function(request, response, next) {
    const userData = request.body;
    db.User.findOne({ where: { id: request.user.id } })
      .then(function(user) {
        if (!user) {
          return response.status(404).json({ message: "Not Found" });
        }
        let hash = user.encryptPassword(userData.current_password, user.salt);
        if (hash !== user.hashedPassword) {
          return response
            .status(422)
            .json([{ field: "password", message: "Wrong password" }]);
        }
        user.name = userData.name || user.name;
        if (userData.new_password && userData.new_password.length > 0) {
          user.password = userData.new_password;
        }
        user.email = userData.email || user.email;

        return user
          .save()
          .then(function(result) {
            return response.status(200).json(user);
          })
          .catch(function(error) {
            if (err.name === "SequelizeValidationError") {
              let errors = err.errors.map(item => ({
                field: item.path,
                message: item.message
              }));
              return response.status(422).json(errors);
            }
            return response.status(500).json();
          });
      })
      .catch(function(err) {
        return next(err);
      });
  },
  me: function(request, response, next) {
    let userId;
    if (request.user) {
      userId = request.user.id;
    }

    db.User.findOne({ where: { id: userId } })
      .then(function(user) {
        if (!user) {
          return response.status(404).json({ message: "Not Found" });
        }
        return response.json(user);
      })
      .catch(function(err) {
        return response.status(400).json(err);
      });
  },
  login: (request, response, next) => {
    const userData = request.body;
    return db.User.find({ where: { email: userData.email } })
    .then(user=>{
      if (!user) {
        return response
          .status(422)
          .json([
            { field: "email", message: "Wrong email" }
          ]);
      }
      let hash = user.encryptPassword(userData.password, user.salt);
      if (hash !== user.hashedPassword) {
        return response
          .status(422)
          .json([
            { field: "password", message: "Wrong password" }
          ]);
      }
      let token = auth.signToken(user.id);
      return response.status(200).json([{ token: token }]);
    })
  },
  userById: (request, response, next) => {
    let userId = parseInt(request.params.id);
    db.User.findOne({ where: { id: userId } })
      .then(function(user) {
        if (!user) {
          return response
            .status(404)
            .json();
        }
        return response.json(user);
      })
      .catch(function(err) {
        return response.status(400).json();
      });
  },
  userSearch: (request, response, next) => {
    let userQuery = request.query;
    let query = {};
    if (userQuery.name) {
      query.name = {$like: `%${userQuery.name}%`}
    }
    if (userQuery.email) {
      query.email = { $like: `%${userQuery.email}%` };
    }
    db.User.findAll({ where: query })
      .then(function(user) {
        if (!user) {
          return response.status(404).json({ message: "Not Found" });
        }
        return response.json(user);
      })
      .catch(function(err) {
        return response.status(400).json(err);
      });
  }
};

module.exports = UserController;

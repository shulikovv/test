/**
 * A place to store local and security sensitive config such as
 * - credentials,
 * - database settings
 * - etc
 */

module.exports = {
  auth: {
    required : true,
  },
  port:3001,
  db: {
    "username": "root",
    "password": null,
    "database": "database_test",
    "host": "127.0.0.1",
    "dialect": "mysql",
    options: {
      port: 3306,
      protocol: 'tcp',
      omitNull: false,
      pool: {
        minConnections: 5,
        maxConnections: 10
        //maxIdleTime: 5000
      },
      define: {
        timestamps: true
      }
    }
  }
};

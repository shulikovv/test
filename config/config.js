const path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development',
    _ = require('lodash');

/**
 * These are basic (default) configuration parameters. They can be overriden.
 */
const config = {
  root: rootPath,
  auth: {
    required : true,
  },
  session: {
    tokenValidityTime: 60*5,
    secret: 'T0p$3cR3t'
  },
  app: {
    name: 'portfolioApi'
  },
  ip: '127.0.0.1',
  port: 3000,
  db: {
    uri: '',
    define: {
      timestamps: true  //this lets sequelize operate by 'createdAt' and 'updatedAt' fields itself
    }
  }

};

/**
 * This is an array of environment-dependent config parameters. Environment name is choosen according to NODE_ENV.
 */
const configEnv = {
  development: {
    db: {
      username: "root",
      password: "_Admin23",
      database: "database_development",
      host: "127.0.0.1",
      dialect: "mysql"
    }
  },
  test: {
    port: 3001,
    db: {
      username: "root",
      password: '_Admin23',
      database: "database_test",
      host: "127.0.0.1",
      dialect: "mysql"
    }
  },
  production: {
    ip: "0.0.0.0",
    db: {} // this must be set in local.js for production mode
  }
};

//this will overload all configs
if(process.env.NODE_ENV === 'test'){
  var localConfig = require(path.resolve('config','test'));
} else {
  localConfig = require(path.resolve('config','local'));
}

module.exports = _.merge(config, configEnv[env], localConfig);

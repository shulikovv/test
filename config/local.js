/**
 * A place to store local and security sensitive config such as
 * - credentials,
 * - database settings
 * - etc
 */

module.exports = {
  auth: {
    required: true
  },
  db: {
    //username: "root",
    //password: "_Admin23",
    //database: "database_development",
    //host: "127.0.0.1",
    //dialect: "mysql",
    options: {
      dialect: "mysql",
      host: "localhost",
      pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
      },
      define: {
        timestamps: true
      }
    }
  }
};

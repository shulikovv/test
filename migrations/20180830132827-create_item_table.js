'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.createTable("items", {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        title: {
          type: Sequelize.STRING(50),
          allowNull: false
        },
        description: {
          type: Sequelize.STRING(255),
          allowNull: true
        },
        image: {
          type: Sequelize.STRING(255),
          allowNull: true
        },
        created_at: {
           type: Sequelize.DATE, 
           defaultValue: Sequelize.NOW
        },
        user_id: {
          type: Sequelize.BIGINT
        }
      });
  },

  down: (queryInterface, Sequelize) => {

      return queryInterface.dropTable('items');
  }
};
